package com.ekiryuhin.TreeMap.domains;

import javafx.beans.property.*;

import javax.persistence.*;

@Entity
@Access(AccessType.FIELD)
public class Tree {

    @Transient private LongProperty id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "treeTypeId")
    private TreeType treeType;

    @Transient private FloatProperty lat;
    @Transient private FloatProperty lng;

    public Tree() {
        id = new SimpleLongProperty();
        lat = new SimpleFloatProperty();
        lng = new SimpleFloatProperty();
    }

    public Tree(TreeType type, float lat, float lng) {
        id = new SimpleLongProperty();
        this.treeType = type;
        this.lat = new SimpleFloatProperty(lat);
        this.lng = new SimpleFloatProperty(lng);
    }

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Access(AccessType.PROPERTY)
    public long getId() {
        return id.get();
    }

    public LongProperty idProperty() {
        return id;
    }

    public void setId(long id) {
        this.id.set(id);
    }


    public String getTreeTypeName() {
        return treeType.getName();
    }

    public TreeType getTreeType() {
        return treeType;
    }

    public void setTreeType(TreeType treeType) {
        this.treeType = treeType;
    }

    @Access(AccessType.PROPERTY)
    public float getLat() {
        return lat.get();
    }

    public FloatProperty latProperty() {
        return lat;
    }

    public void setLat(float lat) {
        this.lat.set(lat);
    }

    @Access(AccessType.PROPERTY)
    public float getLng() {
        return lng.get();
    }

    public FloatProperty lngProperty() {
        return lng;
    }

    public void setLng(float lng) {
        this.lng.set(lng);
    }
}
