package com.ekiryuhin.TreeMap.domains;

import javafx.beans.property.*;

import javax.persistence.*;

@Entity
@Access(AccessType.FIELD)
public class TreeType {


    @Transient private LongProperty treeTypeId;
    @Transient private StringProperty name;
    @Transient private StringProperty description;
    @Transient private BooleanProperty alwaysGreen;

    public TreeType() {
        treeTypeId = new SimpleLongProperty();
        name = new SimpleStringProperty();
        description = new SimpleStringProperty();
        alwaysGreen = new SimpleBooleanProperty();
    }

    public TreeType(long treeTypeId, String name, String description, boolean alwaysGreen) {
        this.treeTypeId = new SimpleLongProperty(treeTypeId);
        this.name = new SimpleStringProperty(name);
        this.description = new SimpleStringProperty(description);
        this.alwaysGreen = new SimpleBooleanProperty(alwaysGreen);
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Access(AccessType.PROPERTY)
    public long getTreeTypeId() {
        return treeTypeId.get();
    }

    public LongProperty treeTypeIdProperty() {
        return treeTypeId;
    }

    public void setTreeTypeId(long treeTypeId) {
        this.treeTypeId.set(treeTypeId);
    }

    public boolean isAlwaysGreen() {
        return alwaysGreen.get();
    }

    @Column(name = "Name", unique = true)
    @Access(AccessType.PROPERTY)
    public String getName() {
        return name.get();
    }

    public StringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    @Access(AccessType.PROPERTY)
    public String getDescription() {
        return description.get();
    }

    public StringProperty descriptionProperty() {
        return description;
    }

    public void setDescription(String description) {
        this.description.set(description);
    }

    @Access(AccessType.PROPERTY)
    public boolean getAlwaysGreen() {
        return alwaysGreen.get();
    }

    public BooleanProperty alwaysGreenProperty() {
        return alwaysGreen;
    }

    public void setAlwaysGreen(boolean alwaysGreen) {
        this.alwaysGreen.set(alwaysGreen);
    }
}
