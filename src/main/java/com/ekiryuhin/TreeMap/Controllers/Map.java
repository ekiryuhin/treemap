package com.ekiryuhin.TreeMap.Controllers;

import com.ekiryuhin.TreeMap.domains.Tree;
import com.ekiryuhin.TreeMap.services.impl.TreeServiceImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Worker;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import rx.Observer;

@Component
public class Map implements Observer<Tree> {

    @Autowired
    TreeServiceImpl treeService;

    @FXML
    private WebView mapView;
    private WebEngine mapEngine;

    private final ObservableList<Tree> trees = FXCollections.observableArrayList();

    public void initialize() {
        treeService.subscribe(this);
        mapEngine = mapView.getEngine();
        setMap();
        viewTrees();
    }

    private void setMap() {
        mapEngine.load(getClass().getResource("/static/templates/map/map.html").toExternalForm());
    }

    private void viewTrees() {
        loadTrees();
        for (Tree tree : trees)
            addTree(tree);
        setCluster();
    }

    private void loadTrees() {
        trees.setAll(treeService.getAll());
    }

    private void update() {
        viewTrees();
    }

    public void addTree(Tree tree) {
        addMarker(tree);
        mapEngine.reload();
    }

    private void setCluster() {
        mapEngine.getLoadWorker().stateProperty().addListener(
                (observable, oldValue, newValue) -> {
                    if (newValue == Worker.State.SUCCEEDED) {
                        String script = "setCluster()";
                        mapEngine.executeScript(script);
                    }
                }
        );
    }

    private void addMarker(Tree tree) {

        float lat = tree.getLat();
        float lng = tree.getLng();

        mapEngine.getLoadWorker().stateProperty().addListener(
                (observable, oldValue, newValue) -> {
                    if (newValue == Worker.State.SUCCEEDED) {

                        ObjectMapper mapper = new ObjectMapper();
                        String JSONLat = null;
                        String JSONLng = null;
                        String JSONMarkerTitle = null;
                        boolean parsingError = false;

                        try {
                            JSONLat = mapper.writeValueAsString(lat);
                            JSONLng = mapper.writeValueAsString(lng);
                            JSONMarkerTitle = mapper.writeValueAsString(tree.getTreeTypeName() + " " + lat + " " + lng);
                        } catch (JsonProcessingException e) {
                            e.printStackTrace();
                            parsingError = true;
                        }

                        String funcName = "setMarker";
                        String script = funcName + "(" + JSONLat + "," + JSONLng + "," + JSONMarkerTitle + ")";

                        if (!parsingError)
                            mapEngine.executeScript(script);
                        else showError();
                    }
                }
        );
    }

    @Override
    public void onCompleted() {

    }

    @Override
    public void onError(Throwable e) {
        showError();
    }

    private void showError() {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setContentText("Error");
        alert.show();
    }

    @Override
    public void onNext(Tree tree) {
        update();
    }
}
