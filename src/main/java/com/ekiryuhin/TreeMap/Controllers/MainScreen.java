package com.ekiryuhin.TreeMap.Controllers;

import com.ekiryuhin.TreeMap.domains.Tree;
import com.ekiryuhin.TreeMap.services.impl.TreeServiceImpl;
import com.ekiryuhin.TreeMap.services.impl.TreeTypeServiceImpl;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MainScreen {

    @FXML
    private JFXComboBox<String> treetypeBox;
    @FXML
    private JFXTextField latField;
    @FXML
    private JFXTextField lngField;
    @FXML
    private TableView treeTable;
    @FXML
    private TableColumn<Tree, Long> idCol;
    @FXML
    private TableColumn<Tree, String> typeCol;
    @FXML
    private TableColumn<Tree, Float> latCol;
    @FXML
    private TableColumn<Tree, Float> lngCol;

    @Autowired
    TreeServiceImpl treeService;
    @Autowired
    TreeTypeServiceImpl treeTypeService;

    public void addTree(ActionEvent actionEvent) {
        treeService.add(new Tree(treeTypeService.getByName(treetypeBox.getValue()), Float.parseFloat(latField.getText()), Float.parseFloat(lngField.getText())));
    }

    public void initialize() {
        bindTable();
        setTreeTypes();
    }

    private void bindTable() {

        idCol.setCellValueFactory(new PropertyValueFactory<>("id"));
        typeCol.setCellValueFactory(new PropertyValueFactory<>("treeTypeName"));
        latCol.setCellValueFactory(new PropertyValueFactory<>("lat"));
        lngCol.setCellValueFactory(new PropertyValueFactory<>("lng"));

        treeTable.setItems(FXCollections.observableArrayList(treeService.getAll()));

    }

    private void setTreeTypes() {
        treetypeBox.setItems(FXCollections.observableArrayList(treeTypeService.getNames()));
    }
}
