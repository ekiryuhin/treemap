package com.ekiryuhin.TreeMap;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class Window extends Application {

    private static Window window;

    private ConfigurableApplicationContext springContext;
    private Stage stage;
    private Scene currentScene;

    public static void main(String[] args) {
        launch(args);
    }

    private void changeScene(Parent root) {
        currentScene = new Scene(root);
        stage.setScene(currentScene);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        springContext = SpringApplication.run(Window.class);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/static/fxml/MainScreen.fxml"));
        loader.setControllerFactory(springContext::getBean);
        Parent root = loader.load();
        stage = new Stage();
        changeScene(root);
        window = this;
        stage.show();
    }

    @Override
    public void stop() {
        springContext.close();
    }

    public static Window getWindow() {
        return window;
    }

    public static void setWindow(Window window) {
        Window.window = window;
    }

    public ConfigurableApplicationContext getSpringContext() {
        return springContext;
    }

    public void setSpringContext(ConfigurableApplicationContext springContext) {
        this.springContext = springContext;
    }

    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public Scene getCurrentScene() {
        return currentScene;
    }

    public void setCurrentScene(Scene currentScene) {
        this.currentScene = currentScene;
    }
}
