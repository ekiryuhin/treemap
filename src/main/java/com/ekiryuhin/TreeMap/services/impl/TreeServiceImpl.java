package com.ekiryuhin.TreeMap.services.impl;

import com.ekiryuhin.TreeMap.domains.Tree;
import com.ekiryuhin.TreeMap.repos.TreeRepo;
import com.ekiryuhin.TreeMap.services.TreeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rx.Observer;
import rx.subjects.PublishSubject;

import java.util.List;


@Service
public class TreeServiceImpl implements TreeService {

    @Autowired
    TreeRepo treeRepo;

    private PublishSubject<Tree> subscribeManager = PublishSubject.create();

    public void subscribe(Observer<Tree> observer) {
        subscribeManager.subscribe(observer);
    }

    public List<Tree> getAll() {
        return treeRepo.findAll();
    }

    @Override
    public void add(Tree tree) {
        treeRepo.save(tree);
        subscribeManager.onNext(tree);
    }

    @Override
    public void delete(Tree tree) {
        treeRepo.delete(tree);
        subscribeManager.onNext(tree);
    }

    @Override
    public Tree getById(long id) {
        return treeRepo.getOne(id);
    }
}
