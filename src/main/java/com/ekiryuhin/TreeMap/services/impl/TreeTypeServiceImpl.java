package com.ekiryuhin.TreeMap.services.impl;

import com.ekiryuhin.TreeMap.domains.TreeType;
import com.ekiryuhin.TreeMap.repos.TreeTypeRepo;
import com.ekiryuhin.TreeMap.services.TreeTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TreeTypeServiceImpl implements TreeTypeService {

    @Autowired
    TreeTypeRepo treeTypeRepo;

    @Override
    public void add(TreeType treeType) {
        treeTypeRepo.save(treeType);
    }

    @Override
    public void delete(TreeType treeType) {
        treeTypeRepo.delete(treeType);
    }

    @Override
    public TreeType getById(long id) {
        return treeTypeRepo.getOne(id);
    }

    @Override
    public String getName(long id) {
        return treeTypeRepo.getOne(id).getName();
    }

    @Override
    public TreeType getByName(String name) {
        return treeTypeRepo.findByName(name);
    }

    @Override
    public List<TreeType> getAll() {
        return treeTypeRepo.findAll();
    }

    @Override
    public List<String> getNames() {
        return treeTypeRepo.getAllNames();
    }
}
