package com.ekiryuhin.TreeMap.services;

import com.ekiryuhin.TreeMap.domains.TreeType;

import java.util.List;

public interface TreeTypeService {

    void add(TreeType treeType);
    void delete(TreeType treeType);
    TreeType getById(long id);
    String getName(long id);
    TreeType getByName(String name);
    List<TreeType> getAll();
    List<String> getNames();

}
