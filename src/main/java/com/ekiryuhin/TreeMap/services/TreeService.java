package com.ekiryuhin.TreeMap.services;

import com.ekiryuhin.TreeMap.domains.Tree;

public interface TreeService {

    void add(Tree tree);
    void delete(Tree tree);
    Tree getById(long id);

}
