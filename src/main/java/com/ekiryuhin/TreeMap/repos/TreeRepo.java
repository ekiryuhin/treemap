package com.ekiryuhin.TreeMap.repos;

import com.ekiryuhin.TreeMap.domains.Tree;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TreeRepo extends JpaRepository<Tree, Long> {

   // String getByTreeTypeName

}
