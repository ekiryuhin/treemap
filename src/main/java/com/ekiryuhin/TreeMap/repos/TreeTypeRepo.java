package com.ekiryuhin.TreeMap.repos;

import com.ekiryuhin.TreeMap.domains.TreeType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface TreeTypeRepo extends JpaRepository<TreeType, Long> {
    TreeType findByName(String name);

    @Query("SELECT name FROM TreeType")
    List<String> getAllNames();
}
