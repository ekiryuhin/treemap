var map;
//var markers = new Map();
var markers;


function setMarker(_lat, _lng, contentText) {

    let posi = new google.maps.LatLng(_lat, _lng);
    let image = './icons/DefaultTreeIcon.png';

    let marker = new google.maps.Marker({
        position: posi,
        map: map,
        icon: image
    });

    markers.set(posi, marker);
  //  markers.push(marker);

    let contentString = '<h2>' + contentText + '</h2>';
    let infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    marker.addListener('click', function () {
        infowindow.open(map, marker);
    });

}

function setCluster() {
    let mcOptions = {gridSize: 50, maxZoom: 15, imagePath: './icons/TreeClusterIcon'};
    let markerClusterer = new MarkerClusterer(map, Array.from(markers.values()), mcOptions);
   // var markerClusterer = new MarkerClusterer(map, markers, mcOptions);
}

function initMap() {
    let lat = 55.751697;
    let lng = 37.616776;
    let centrPosi = new google.maps.LatLng(lat, lng);
    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 7,
        center: centrPosi
    });
    markers = new Map();
}





